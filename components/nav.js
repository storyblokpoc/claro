import React from 'react'
import Link from 'next/link'

const Nav = () => (
  <nav>
    <header id="main-header">
      <div className="container-header d-header">
        <div className="skip-link hide-skip-link is-skip-link" id="skip-link">
          <ul className="container">
            <li>
              <a tabIndex="1" className="is-skip-link gtm-link-event" href="#main-content" title="Ir para o corpo do site" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Ir para o Cabeçalho">Ir para o corpo do site</a>
            </li>
            <li>
              <a tabIndex="2" className="is-skip-link gtm-link-event" href="#nav-menu" title="Ir para o Cabeçalho" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Ir para o Cabeçalho">Ir para o Cabeçalho</a>
            </li>
            <li>
              <a tabIndex="3" className="is-skip-link gtm-link-event" href="#main-footer" title="Ir para o Rodapé" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Ir para o Cabeçalho">Ir para o Rodapé</a>
            </li>
          </ul>
        </div>
        <div className="nav-institucional">
          <div className="container no-gutters bd-bt">
            <nav className="flex__container" id="topo">
              <ul className="container-lista nav-left flex__item">
                <li className="tab-nav nav-global-item active">
                  <span>Para você e sua família</span>
                </li>
                <li className="tab-nav nav-global-item">
                  <a href="https://www.claro.com.br/empresas" title="Conheça os produtos e serviços Claro para sua empresa." className="gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Conheça os produtos e serviços Claro para sua empresa.">Para sua empresa</a>
                </li>
              </ul>
              <ul className="container-lista nav-right flex__item">
                <li className="nav-global-item">
                  <a href="https://www.claro.com.br/institucional/regulatorio/acessibilidade" target="_self" title="Acessibilidade" className="gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Pagina Acessibilidade">
                  <i className="mdn-Icon-deficiencia mdn-Icon--sm mdn-Icon--sm" aria-hidden="true"></i>
                  <span className="icon-acessible">Acessibilidade</span>
                  </a>
                </li>
                <li className="nav-global-item">
                  <a href="#" id="altoContraste" className="highContrast gtm-link-event" aria-hidden="true" title="Contraste" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Contraste">
                  <i className="mdn-Icon-autocontraste mdn-Icon--sm" aria-hidden="true"></i>
                  <span className="icon-acessible">Contraste</span>
                  </a>
                </li>
                <li className="nav-global-item">
                  <a href="#" className="gtm-link-event action-zoom" title="A mais e A menos para dar foco ou diminuir o foco" aria-hidden="true" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="A mais e A menos para dar foco ou diminuir o foco">
                  <i className="mdn-Icon-aumentar-tamanho mdn-Icon--sm mdn-Icon--sm" aria-hidden="true"></i>
                  <i className="mdn-Icon-diminuir-tamanho mdn-Icon--sm mdn-Icon--sm" aria-hidden="true"></i>
                  </a>
                </li>
                <li className="nav-global-item status-cliente container-menu-drop-dow visually-hidden" aria-hidden="true">
                  <a href="#" className="menu-drop-dow gtm-link-event" title="Selecione a área" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="trocar status cliente">
                  <span className="texto-padrao">Área:</span>
                  <strong className="texto-status"></strong>
                  <i className="fa fa-angle-down icones-fontsize" aria-hidden="true"></i>
                  </a>
                  <div className="status-trocar caixa-padrao">
                    <a href="#" className="status-link gtm-link-event" title="Alterar cidade" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="trocar status">
                    <span className="status-trocar-titulo">Trocar para:</span>
                    <strong className="status-trocar-texto"></strong>
                    </a>
                  </div>
                </li>
                <li className="nav-global-item estado-cliente cidade-default container-menu-drop-dow" style={{opacity: 1}}>
                  <a href="#" className="cidades menu-drop-dow gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/nao-cliente" data-gtm-event-label="troca de cidade" aria-label="As informações do portal referem-se à cidade de São Paulo/SP,entre na área expansível para alterá-la" aria-expanded="false" role="button">
                  <i className="ico-pre fa mdn-Icon-localizacao icones-fontsize" aria-hidden="true"></i>
                  <span className="target-cidades" aria-hidden="true">São Paulo/SP</span>
                  <i className="fa fa-angle-down icones-fontsize" aria-hidden="true"></i>
                  </a>
                  <div className="selectCidade target-list caixa-padrao">
                    <form id="formPreHomeTopBar" className="requestJson">
                      <fieldset>
                        <legend className="sr-only" aria-hidden="true" tabIndex="-1">Escolha sua cidade.</legend>
                        <label htmlFor="getValueAutocompleteTopBar" aria-hidden="true" tabIndex="-1">Você está em:</label>
                        <input id="getValueAutocompleteTopBar" className="inputAutocomplete ui-autocomplete-input" placeholder="Exemplo: São Paulo/SP" aria-label="Digite o nome da cidade" autoComplete="off" />
                        <button type="submit" className="alterar-cookie gtm-element-event" data-home-type="cliente" data-gtm-event-action="alterar-cidade:click" data-gtm-event-category="claro:portal:/nao-cliente" data-gtm-event-label="alterar">Alterar cidade</button>
                      </fieldset>
                    </form>
                    <ul id="ui-id-1" tabIndex="0" className="ui-menu ui-widget ui-widget-content ui-autocomplete ui-front" style={{display: 'none'}}></ul>
                  </div>
                </li>
                <li className="nav-global-item bandeira">
                  <div className="container-menu-drop-dow">
                    <a href="#" className="bandeira-itens menu-drop-dow gtm-link-event" title="Escolha o país" aria-expanded="false" role="button" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="escolha o país">
                    <img src="https://www.claro.com.br/static/images/icon/bandeiras/brasil.png" alt="Bandeira do Brasil" aria-hidden="true" />
                    <i className="fa fa-angle-down icones-fontsize" aria-hidden="true"></i>
                    <span className="sr-only">Confira os sites da Claro pelo mundo</span>
                    </a>
                    <div className="nav-world caixa-padrao">
                      <ul className="nav-world-list">
                        <li>
                          <a href="https://www.claro.com.ar/personas/" target="_self" className="gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Argentina">
                          <span className="bandeira-itens argentina icones-fontsize"></span> Argentina
                          </a>
                        </li>
                        <li>
                          <a href="http://www.clarochile.cl/personas/" target="_self" className="gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Chile">
                          <span className="bandeira-itens chile icones-fontsize"></span> Chile
                          </a>
                        </li>
                        <li>
                          <a href="http://www.claro.com.co/personas/" target="_self" className="gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Colombia">
                          <span className="bandeira-itens colombia icones-fontsize"></span> Colombia
                          </a>
                        </li>
                        <li>
                          <a href="https://www.claro.cr/personas/" target="_self" className="gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Costa Rica">
                          <span className="bandeira-itens costa-rica icones-fontsize"></span> Costa Rica
                          </a>
                        </li>
                        <li>
                          <a href="http://www.claro.com.sv/personas/" target="_self" className="gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="El Salvador">
                          <span className="bandeira-itens el-salvador icones-fontsize"></span> El Salvador
                          </a>
                        </li>
                        <li>
                          <a href="https://www.claro.com.gt/personas/" target="_self" className="gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Guatemala">
                          <span className="bandeira-itens guatemala icones-fontsize"></span> Guatemala
                          </a>
                        </li>
                        <li>
                          <a href="https://www.claro.com.hn/personas/" target="_self" className="gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Honduras">
                          <span className="bandeira-itens honduras icones-fontsize"></span> Honduras
                          </a>
                        </li>
                        <li>
                          <a href="https://www.claro.com.ni/personas/" target="_self" className="gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Nicaragua">
                          <span className="bandeira-itens nicaragua icones-fontsize"></span> Nicaragua
                          </a>
                        </li>
                        <li>
                          <a href="http://www.claro.com.pa/personas/" target="_self" className="gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Panamá">
                          <span className="bandeira-itens panama icones-fontsize"></span> Panamá
                          </a>
                        </li>
                        <li>
                          <a href="http://www.claro.com.py/personas/" target="_self" className="gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Paraguay">
                          <span className="bandeira-itens paraguai icones-fontsize"></span> Paraguay
                          </a>
                        </li>
                        <li>
                          <a href="http://www.claro.com.pe/personas/" target="_self" className="gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Perú">
                          <span className="bandeira-itens peru icones-fontsize"></span> Perú
                          </a>
                        </li>
                        <li>
                          <a href="https://www.claropr.com/portal/pr/sc/personas/" target="_self" className="gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Puerto Rico">
                          <span className="bandeira-itens porto-rico icones-fontsize"></span> Puerto Rico
                          </a>
                        </li>
                        <li>
                          <a href="http://www.claro.com.do/personas/" target="_self" className="gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="República Dominicana">
                          <span className="bandeira-itens republica-dominicana icones-fontsize"></span> República Dominicana
                          </a>
                        </li>
                        <li>
                          <a href="https://www.claro.com.uy/personas/" target="_self" className="gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Uruguay">
                          <span className="bandeira-itens uruguai icones-fontsize"></span> Uruguay
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <div id="nav-menu">
          <div className="menu-principal" id="menu-principal">
            <nav className="flex__container">
              <div className="container-logo-claro flex__item logo">
                <a href="https://www.claro.com.br" title="Logo Claro" className="gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Logo Claro">
                <img src="https://www.claro.com.br/imagem/logo-claro-1509138305180.png" alt="A marca Claro é escrita com letras na cor branca. Na letra “o” há representação de um sol, com três traços retangulares na parte superior direita, simbolizando seus raios. " title="Logo Claro" />
                </a>
              </div>
              <ul className="container-lista lista-servicos flex__item" aria-label="Menu principal, digite shift + S, para expandir os itens expansíveis">
                <li className="nav-global-item max-width-nav ">
                  <a href="https://planos.claro.com.br" className="nav-tab-link gtm-link-event " target="_self" title="Monte o seu combo Claro com celular, internet, TV e fone." data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Combos">
                  Combos
                  </a>
                </li>
                <li className="nav-global-item max-width-nav ">
                  <a href="https://www.claro.com.br/celular" className="nav-tab-link gtm-link-event " target="_self" aria-expanded='false' title="Conheça os planos pós, controle e pré para celular." data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Móvel">
                  Móvel
                  </a>
                  <ul className="nav-submenu">
                    <li className="nav-submenu-item-cel col-text">
                      <ul className="item-esquerda">
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/celular/plano-pos" title="Conheça os planos pós da Claro para celular." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Móvel:Planos Pós">Planos Pós</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/celular/controle" title="Conheça os planos controle da Claro para celular." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Móvel:Planos Controle">Planos Controle</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/celular/flex" title="Conheça os planos flex da Claro para o seu celular." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Móvel:Planos Flex">Planos Flex</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/celular/planos-pre" title="Conheça o Prezão e demais promoções Pré da Claro para celular." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Móvel:Promoções Pré">Promoções Pré</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/celular/portabilidade" title="Venha para a Claro! Saiba tudo sobre a portabilidade numérica." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Móvel:Portabilidade">Portabilidade</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/celular/pos/passaporte" title="Utilize seu plano de celular no exterior como se estivesse no Brasil." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Móvel:Uso no exterior">Uso no exterior</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/cr" title="Não fique sem créditos! Faça uma recarga para o seu pré, controle ou TV." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Móvel:Recarga">Recarga</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/cobertura" title="Conheça a cobertura de sinal 4G e 3G da Claro." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Móvel:Cobertura">Cobertura</a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li className="nav-global-item max-width-nav ">
                  <a href="https://www.claro.com.br/internet" className="nav-tab-link gtm-link-event " target="_self" aria-expanded='false' title="Conheça os planos e soluções de internet da Claro." data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Internet">
                  Internet
                  </a>
                  <ul className="nav-submenu">
                    <li className="nav-submenu-item-cel col-text">
                      <ul className="item-direita">
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/internet/banda-larga" title="Conheça os planos de internet fixa." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Internet:Planos internet fixa">Planos internet fixa</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/internet/banda-larga/wifi-dentro-de-casa" title="Conheça as soluções Claro de Wi-Fi para dentro da sua casa." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Internet:Wi-Fi dentro de casa">Wi-Fi dentro de casa</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/internet/banda-larga/servicos-adicionais" title="Conheça as soluções de internet da Claro para turbinar a sua rede de casa." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Internet:Serviços adicionais">Serviços adicionais</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/internet/movel" title="Conheça os planos de internet móvel." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Internet:Planos internet móvel">Planos internet móvel</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/internet/rede-net-claro-wifi" title="Fique conectado até mesmo fora de casa. Conheça a rede #NET-CLARO-WIFI." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Internet:Wi-Fi fora de casa">Wi-Fi fora de casa</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/cobertura" title="Conheça a cobertura de sinal 4G e 3G da Claro." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Internet:Cobertura">Cobertura</a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li className="nav-global-item max-width-nav ">
                  <a href="https://www.claro.com.br/tv-por-assinatura" className="nav-tab-link gtm-link-event " target="_self" aria-expanded='false' title="Conheça os planos de TV da Claro." data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="TV">
                  TV
                  </a>
                  <ul className="nav-submenu">
                    <li className="nav-submenu-item-cel col-text">
                      <ul className="item-esquerda">
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/tv-por-assinatura" title="Conheça os planos de TV da Claro." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="TV:Planos">Planos</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/tv-por-assinatura/recursos" title="Conheça todos os recursos que você pode ter na sua Claro net tv." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="TV:Recursos da TV">Recursos da TV</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/tv-por-assinatura/programacao" title="Saiba tudo o que está passando na programação da sua TV Claro." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="TV:Programação">Programação</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/tv-por-assinatura/turbine" title="Turbine a sua TV com canais à la carte." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="TV:Turbine sua TV">Turbine sua TV</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/tv-por-assinatura/pre-pago" title="Conheça a Claro tv pré-pago." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="TV:Claro tv pré-pago">Claro tv pré-pago</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/now" title="Clique aqui no menu Serviços Digitais e conheça o NOW. O melhor do entretenimento onde e quando quiser." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="TV:NOW">NOW</a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li className="nav-global-item max-width-nav ">
                  <a href="https://www.claro.com.br/fone" className="nav-tab-link gtm-link-event " target="_self" title="Conheça os planos de telefone fixo da Claro." data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Fone">
                  Fone
                  </a>
                </li>
                <li className="nav-global-item max-width-nav ">
                  <a href="https://www.claro.com.br/servicos" className="nav-tab-link gtm-link-event " target="_self" aria-expanded='false' title="Conheça os serviços digitais da Claro." data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Serviços Digitais">
                  Serviços Digitais
                  </a>
                  <ul className="nav-submenu">
                    <li className="nav-submenu-item-cel col-text">
                      <ul className="item-direita">
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/servicos/educacao" title="Conheça os serviços digitais de educação da Claro." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Serviços Digitais:Educação">Educação</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/servicos/entretenimento" title="Conheça os serviços digitais de entretenimento da Claro." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Serviços Digitais:Entretenimento">Entretenimento</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/servicos/financeiro" title="Conheça os serviços digitais de financeiro da Claro." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Serviços Digitais:Financeiro">Financeiro</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/servicos/kids" title="Conheça os serviços digitais de kids da Claro." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Serviços Digitais:Kids">Kids</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/kidson" title="Conheça relógio celular da Claro que conecta você ao seu filho." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Serviços Digitais:Kids On">Kids On</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/now" title="Clique aqui no menu Serviços Digitais e conheça o NOW. O melhor do entretenimento onde e quando quiser." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Serviços Digitais:NOW">NOW</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/servicos/saude-e-bem-estar" title="Conheça os serviços digitais de saúde e bem-estar da Claro." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Serviços Digitais:Saúde e bem-estar">Saúde e bem-estar</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/servicos/seguranca" title="Conheça os serviços digitais de segurança da Claro." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Serviços Digitais:Segurança">Segurança</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/servicos/utilidades" title="Conheça os serviços digitais de utilidades da Claro." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Serviços Digitais:Utilidades">Utilidades</a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li className="nav-global-item max-width-nav ">
                  <a href="https://www.claro.com.br/atendimento" className="nav-tab-link gtm-link-event " target="_self" aria-expanded='false' title="Resolva sua vida sem sair de casa. Conheça todas as soluções de autoatendimento da Claro." data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Atendimento">
                  Atendimento
                  </a>
                  <ul className="nav-submenu">
                    <li className="nav-submenu-item-cel col-text">
                      <ul className="item-direita">
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/atendimento/debito-automatico-e-fatura-digital" title="Facilite sua vida e receba a sua fatura por e-mail." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Atendimento:Fatura digital">Fatura digital</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/atendimento/fale-com-a-claro" title="Conheça todos os canais de atendimento da Claro." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Atendimento:Fale com a Claro">Fale com a Claro</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/atendimento/perguntas-frequentes" title="Tire suas dúvidas sobre os produtos e serviços Claro." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Atendimento:Perguntas frequentes">Perguntas frequentes</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://www.claro.com.br/encontre-uma-loja" title="Encontre uma loja Claro mais perto de você." target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Atendimento:Encontre uma loja">Encontre uma loja</a>
                        </li>
                        <li className="nav-submenu-item">
                          <a href="https://chat.claro.com.br/csp-magent-client/login.jsp?aplicacaoOrigem=movelcic" title="Atendimento para deficiente auditivo" target="_self" className=" gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Atendimento:Deficiente auditivo">Deficiente auditivo</a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li className="nav-global-item max-width-nav ">
                  <a href="https://www.claro.com.br/claro-clube" className="nav-tab-link gtm-link-event " target="_self" title="Conheça o programa de relacionamento Claro clube e todos os benefícios em ser cliente Claro." data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Claro clube">
                  Claro clube
                  </a>
                </li>
                <li className="c-header-btn submenu-active contrate container-menu-drop-dow" data-submenu-id="submenu-contrate">
                  <a href="#" className="gtm-link-event c-header-btn-item menu-drop-dow  cor3 " title="Escolha o produto Claro que deseja contratar!" target="_self" aria-expanded='false' data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Contrate">
                  Contrate
                  </a>
                  <ul id="submenu-contrate" className="caixa-padrao submenu-list popover active-hover">
                    <li className="btn-close-submenu">
                      <i className="fa fa-times icones-fontsize"></i>
                    </li>
                    <li className="submenu-list__item">
                      <a href="https://planos.claro.com.br" title="Clique aqui e monte um combo Claro de acordo com a sua necessidade." target="_self" className="submenu-list__item--link gtm-link-event icones-fontsize" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Contrate:Clique aqui e monte um combo Claro de acordo com a sua necessidade.">
                      <i className="fa mdn-Icon-carrinho icones-fontsize"></i>
                      <span className="text">Combos Monte um combo de acordo com a sua necessidade.</span>
                      </a>
                    </li>
                    <li className="submenu-list__item">
                      <a href="https://assine.claro.com.br/?trks=portalclaro" title="Planos para celular, fale ilimitado e navegue com a internet até 10x mais rápida." target="_self" className="submenu-list__item--link gtm-link-event icones-fontsize" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Contrate:Planos para celular, fale ilimitado e navegue com a internet até 10x mais rápida.">
                      <i className="fa mdn-Icon-celular icones-fontsize"></i>
                      <span className="text">Planos para celular Fale ilimitado e navegue com a internet até 10x mais rápida.</span>
                      </a>
                    </li>
                    <li className="submenu-list__item">
                      <a href="https://planos.claro.com.br" title="Internet, conheça os planos de ultravelocidade para a sua casa." target="_self" className="submenu-list__item--link gtm-link-event icones-fontsize" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Contrate:Internet, conheça os planos de ultravelocidade para a sua casa.">
                      <i className="fa mdn-Icon-internet icones-fontsize"></i>
                      <span className="text">Internet Planos de ultravelocidade para a sua casa.</span>
                      </a>
                    </li>
                    <li className="submenu-list__item">
                      <a href="https://planos.claro.com.br" title="Planos para TV, A mais completa plataforma de conteúdo para você curtir em todas as telas." target="_self" className="submenu-list__item--link gtm-link-event icones-fontsize" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Contrate:Planos para TV, A mais completa plataforma de conteúdo para você curtir em todas as telas.">
                      <i className="fa mdn-Icon-tv icones-fontsize"></i>
                      <span className="text">TV HD A mais completa plataforma de conteúdo para você curtir em todas as telas.</span>
                      </a>
                    </li>
                    <li className="submenu-list__item">
                      <a href="https://lojaonline.claro.com.br/" title="Os melhores aparelhos em ofertas estão aqui." target="_self" className="submenu-list__item--link gtm-link-event icones-fontsize" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Contrate:Os melhores aparelhos em ofertas estão aqui.">
                      <i className="fa mdn-Icon-carrinho icones-fontsize"></i>
                      <span className="text">Smartphones Os melhores aparelhos em ofertas estão aqui.</span>
                      </a>
                    </li>
                    <li className="submenu-list__item">
                      <a href="https://clarorecarga.claro.com.br/" title="Não fique sem créditos, faça uma recarga para seu pré, controle ou tv." target="_self" className="submenu-list__item--link gtm-link-event icones-fontsize" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Contrate:Não fique sem créditos, faça uma recarga para seu pré, controle ou tv.">
                      <i className="fa mdn-Icon-recarga icones-fontsize"></i>
                      <span className="text">Recarga Não fique sem créditos, faça uma recarga.</span>
                      </a>
                    </li>
                    <li className="submenu-list__item">
                      <a href="https://www.claro.com.br/empresas" title="Clique aqui para ser direcionado para o site Claro empresas e conhecer os planos para a sua empresa." target="_self" className="submenu-list__item--link gtm-link-event icones-fontsize" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Contrate:Clique aqui para ser direcionado para o site Claro empresas e conhecer os planos para a sua empresa.">
                      <i className="fa mdn-Icon-empresa icones-fontsize"></i>
                      <span className="text">Para sua empresa Monte um combo para sua empresa.</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
              <ul className="container-lista lista-cliente flex__item nav-right">
                <li className="nav-global-item icon-acessible busca submenu-active" data-submenu-id="submenu-busca">
                  <a href="#" className="fa menu-drop-dow gtm-link-event icones-fontsize" title="O que deseja procurar?" aria-expanded="false" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="o-que-deseja-procurar">
                  <i className="mdn-Icon-busca mdn-Icon--md" aria-hidden="true"></i>
                  </a>
                  <div className="caixa-padrao" id="submenu-busca">
                    <span className="btn-close-submenu">
                    <i className="fa fa-times icones-fontsize"></i>
                    </span>
                    <form id="formBusca" action="https://www.claro.com.br/resultado-de-busca" className="form-busca search-submit-verify">
                      <fieldset className="actions-form">
                        <legend className="hide">O que deseja procurar</legend>
                        <label className="button-formBusca" htmlFor="button-formBusca" aria-hidden="true">O que deseja procurar</label>
                        <div className="container-busca-buttons">
                          <input id="button-formBusca" type="text" name="q" />
                          <button type="submit" className="fa mdn-Icon-busca disabled-button search-target-verify icones-fontsize" disabled><i>O que deseja procurar</i></button>
                        </div>
                      </fieldset>
                    </form>
                  </div>
                </li>
                <li className="nav-global-item icon-acessible televendas submenu-active" data-submenu-id="submenu-televendas">
                  <a href="#" className="fa  menu-drop-dow gtm-link-event icones-fontsize" target="_blank" title="Telefones" aria-expanded="false" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="">
                  <i className=" mdn-Icon-telefone-fixo mdn-Icon--md " aria-hidden="true"></i>
                  </a>
                  <div className="caixa-padrao popover" id="submenu-televendas">
                    <span className="btn-close-submenu">
                    <i className="fa fa-times icones-fontsize"></i>
                    </span>
                    <ul>
                      <li className="item">
                        <strong> Televendas: 0800 720 1234</strong>
                      </li>
                    </ul>
                  </div>
                </li>
                <li className="c-header-btn submenu-active claro container-menu-drop-dow minhaClaro" data-submenu-id="submenu-minhaClaro">
                  <a href="#" title="Clique aqui no cabeçalho e conheça todas as facilidades que o Minha Claro oferece." className="gtm-link-event c-header-btn-item menu-drop-dow  cor6 " target="_self" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Minha Claro">
                  Minha Claro
                  </a>
                  <ul id="submenu-minhaClaro" className="caixa-padrao submenu-list popover active-hover">
                    <li className="btn-close-submenu">
                      <i className="fa fa-times icones-fontsize"></i>
                    </li>
                    <li className="submenu-list__item">
                      <a href="https://www.claro.com.br/mc" title="Clique aqui e aproveite todas as facilidades que o Minha Claro móvel oferece." target="_self" className="submenu-list__item--link gtm-link-event icones-fontsize" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Minha Claro:Clique aqui e aproveite todas as facilidades que o Minha Claro móvel oferece.">
                      <i className="fa mdn-Icon-celular icones-fontsize"></i>
                      <span className="text">Minha Claro móvel</span>
                      </a>
                    </li>
                    <li className="submenu-list__item">
                      <a href="https://www.claro.com.br/mcr" title="Clique aqui e aproveite todas as facilidades que o Minha Claro residencial (antigo Minha NET) oferece." target="_self" className="submenu-list__item--link gtm-link-event icones-fontsize" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Minha Claro:Clique aqui e aproveite todas as facilidades que o Minha Claro residencial (antigo Minha NET) oferece.">
                      <i className="fa mdn-Icon-residencial icones-fontsize"></i>
                      <span className="text">Minha Claro residencial Antigo Minha NET</span>
                      </a>
                    </li>
                    <li className="submenu-list__item">
                      <a href="https://minhaclarotv.claro.com.br" title="Clique aqui e aproveite todas as facilidades que o Minha Claro residencial (antigo Minha Claro tv) oferece." target="_self" className="submenu-list__item--link gtm-link-event icones-fontsize" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Minha Claro:Clique aqui e aproveite todas as facilidades que o Minha Claro residencial (antigo Minha Claro tv) oferece.">
                      <i className="fa mdn-Icon-residencial icones-fontsize"></i>
                      <span className="text">Minha Claro residencial Antigo Minha Claro tv</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
          <div className="menu-interno flex__container">
            <ul className="container-menu nav-left flex__item breadcrumb">
              <li className="item titulo-principal">
                <a href="https://www.claro.com.br/celular/portabilidade" title="Portabilidade" target="_self" className="item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Portabilidade">Portabilidade</a>
              </li>
            </ul>
            <ul className="container-menu nav-right flex__item flex__right">
              <li className="item">
                <a href="https://www.claro.com.br/celular/plano-pos" title="Planos Pós" target="_self" className="item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Planos Pós">Planos Pós</a>
              </li>
              <li className="item">
                <a href="https://www.claro.com.br/celular/controle" title="Planos Controle" target="_self" className="item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Planos Controle">Planos Controle</a>
              </li>
              <li className="item">
                <a href="https://www.claro.com.br/celular/flex" title="Planos Flex" target="_self" className="item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Planos Flex">Planos Flex</a>
              </li>
              <li className="item">
                <a href="https://www.claro.com.br/celular/planos-pre" title="Promoções Pré" target="_self" className="item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Promoções Pré">Promoções Pré</a>
              </li>
              <li className="item">
                <a href="https://www.claro.com.br/celular/pos/passaporte" title="Uso no exterior" target="_self" className="item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Uso no exterior">Uso no exterior</a>
              </li>
              <li className="item">
                <a href="https://www.claro.com.br/cr" title="Recarga" target="_self" className="item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Recarga">Recarga</a>
              </li>
              <li className="item">
                <a href="https://www.claro.com.br/cobertura" title="Cobertura" target="_self" className="item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Cobertura">Cobertura</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div className="m-header">
        <div className="m-header-wrapper">
          <span id="collapse" data-list="m-container-menu" className="mdn-Icon-menu mdn-Icon--md btn-open-submenu m-button-collapse"></span>
          <a href="https://www.claro.com.br" title="Logo Claro" className="gtm-link-event m-header-logo" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Logo Claro">
          <img src="https://www.claro.com.br/imagem/logo-claro-1509138305180.png" alt="A marca Claro é escrita com letras na cor branca. Na letra “o” há representação de um sol, com três traços retangulares na parte superior direita, simbolizando seus raios. " title="Logo Claro" />
          </a>
          <div className="c-header-btn">
            <a href="#" data-list="m-container-contrate" title="Escolha o produto Claro que deseja contratar!" className="c-header-btn-item btn-open-submenu gtm-link-event bt-contrate-mobile cor3 header-btn-contrate" target="_self" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Contrate">
            Contrate
            </a>
            <a href="#" data-list="m-container-minha-claro" title="Clique aqui no cabeçalho e conheça todas as facilidades que o Minha Claro oferece." className="c-header-btn-item btn-open-submenu gtm-link-event bt-minhaClaro-mobile cor6 header-btn-contrate" target="_self" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Minha Claro">
            Minha Claro
            </a>
          </div>
        </div>
        <div className="m-header-search m-nao-client">
          <form className="search-submit-verify" action="https://www.claro.com.br/resultado-de-busca">
            <fieldset>
              <legend className="hide">Pesquisar</legend>
              <label className="hide" htmlFor="m-botao-busca">Pesquisar</label>
              <input id="m-botao-busca" type="text" name="q" placeholder="Pesquisar" />
              <button type="submit" className="m-header-search-btn fa mdn-Icon-busca disabled-button search-target-verify icones-fontsize" disabled></button>
            </fieldset>
          </form>
        </div>
        <div className="m-header-menu m-client">
          <ul className="m-menu-list">
            <li className="m-menu-item title icon-toggle icones-fontsize" style={{position: 'relative'}}>
              <a href="https://www.claro.com.br/celular/portabilidade" title="Portabilidade" className="m-menu-item-link gtm-link-event" id="submenu-category" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Portabilidade">Portabilidade</a>
              <i className="fa fa-chevron-up icones-fontsize" aria-hidden="true"></i>
              <i className="fa fa-chevron-down icones-fontsize" aria-hidden="true"> </i>
            </li>
          </ul>
          <ul className="m-header-submenu">
            <li className="m-submenu-item">
              <a href="https://www.claro.com.br/celular" title="Móvel" target="_self" className="m-submenu-item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Móvel">Móvel</a>
            </li>
            <li className="m-submenu-item">
              <a href="https://www.claro.com.br/celular/plano-pos" title="Planos Pós" target="_self" className="m-submenu-item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Planos Pós">Planos Pós</a>
            </li>
            <li className="m-submenu-item">
              <a href="https://www.claro.com.br/celular/controle" title="Planos Controle" target="_self" className="m-submenu-item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Planos Controle">Planos Controle</a>
            </li>
            <li className="m-submenu-item">
              <a href="https://www.claro.com.br/celular/flex" title="Planos Flex" target="_self" className="m-submenu-item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Planos Flex">Planos Flex</a>
            </li>
            <li className="m-submenu-item">
              <a href="https://www.claro.com.br/celular/planos-pre" title="Promoções Pré" target="_self" className="m-submenu-item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Promoções Pré">Promoções Pré</a>
            </li>
            <li className="m-submenu-item">
              <a href="https://www.claro.com.br/celular/pos/passaporte" title="Uso no exterior" target="_self" className="m-submenu-item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Uso no exterior">Uso no exterior</a>
            </li>
            <li className="m-submenu-item">
              <a href="https://www.claro.com.br/cr" title="Recarga" target="_self" className="m-submenu-item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Recarga">Recarga</a>
            </li>
            <li className="m-submenu-item">
              <a href="https://www.claro.com.br/cobertura" title="Cobertura" target="_self" className="m-submenu-item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Cobertura">Cobertura</a>
            </li>
          </ul>
        </div>
      </div>
      <nav className="m-navgation" id="m-nav-menu">
        <div className="m-fixed">
          <div className="m-header-wrapper">
            <span className="fa icon-close m-button-collapse icones-fontsize" id="collapse-close"></span>
            <a href="https://www.claro.com.br" title="Logo Claro" className="gtm-link-event m-header-logo" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Logo Claro">
            <img src="https://www.claro.com.br/imagem/logo-claro-1509138305180.png" alt="A marca Claro é escrita com letras na cor branca. Na letra “o” há representação de um sol, com três traços retangulares na parte superior direita, simbolizando seus raios. " title="Logo Claro" />
            </a>
            <div className="c-header-btn">
              <a href="https://assine.claro.com.br/?trks=portalclaro" title="Escolha o produto Claro que deseja contratar!" className="gtm-link-event c-header-btn-item cor3 header-btn-contrate" target="_self" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="">
              Contrate
              </a>
              <a href="https://www.claro.com.br/minha-claro" title="Clique aqui no cabeçalho e conheça todas as facilidades que o Minha Claro oferece." className="gtm-link-event c-header-btn-item cor6 header-btn-contrate" target="_self" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="">
              Minha Claro
              </a>
            </div>
          </div>
        </div>
        <div className="m-scrolling">
          <ul className="container-lists">
            <li className="m-container-menu list-submenu">
              <div className="m-client-wrapper">
                <div className="m-block-client target-list">
                  <span className="m-block-client-item targetCidadeMobile">
                  <i className="fa mdn-Icon-localizacao icones-fontsize"></i> <strong className="targetCookieCidadeMobile"></strong>
                  </span>
                  <span className="c-header-btn">
                  <span className="c-header-btn-item gray" id="box-city">
                  trocar
                  </span>
                  </span>
                </div>
                <div className="m-block-client target-cliente visually-hidden" aria-hidden="true">
                  <span className="m-block-client-item status-cliente">
                  área: <strong></strong>
                  </span>
                  <span className="c-header-btn status-cliente">
                  <a href="#" title="Trocar" className="c-header-btn-item gray cliente-status gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="trocar">
                  trocar
                  </a>
                  </span>
                </div>
              </div>
              <ul className="m-navgation-list breadcrumb submenu-menu header-btn-contrate">
                <li className="m-navgation-item ">
                  <a href="https://planos.claro.com.br" title="Monte o seu combo Claro com celular, internet, TV e fone." target="_self" className="m-navgation-item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Monte o seu combo Claro com celular, internet, TV e fone.">Combos</a>
                </li>
                <li className="m-navgation-item ">
                  <a href="https://www.claro.com.br/celular" title="Conheça os planos pós, controle e pré para celular." target="_self" className="m-navgation-item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Conheça os planos pós, controle e pré para celular.">Móvel</a>
                </li>
                <li className="m-navgation-item ">
                  <a href="https://www.claro.com.br/internet" title="Conheça os planos e soluções de internet da Claro." target="_self" className="m-navgation-item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Conheça os planos e soluções de internet da Claro.">Internet</a>
                </li>
                <li className="m-navgation-item ">
                  <a href="https://www.claro.com.br/tv-por-assinatura" title="Conheça os planos de TV da Claro." target="_self" className="m-navgation-item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Conheça os planos de TV da Claro.">TV por assinatura</a>
                </li>
                <li className="m-navgation-item ">
                  <a href="https://www.claro.com.br/fone" title="Conheça os planos de telefone fixo da Claro." target="_self" className="m-navgation-item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Conheça os planos de telefone fixo da Claro.">Fone</a>
                </li>
                <li className="m-navgation-item ">
                  <a href="https://www.claro.com.br/servicos" title="Conheça os serviços digitais da Claro." target="_self" className="m-navgation-item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Conheça os serviços digitais da Claro.">Serviços Digitais</a>
                </li>
                <li className="m-navgation-item ">
                  <a href="https://www.claro.com.br/atendimento" title="Resolva sua vida sem sair de casa. Conheça todas as soluções de autoatendimento da Claro." target="_self" className="m-navgation-item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Resolva sua vida sem sair de casa. Conheça todas as soluções de autoatendimento da Claro.">Atendimento</a>
                </li>
                <li className="m-navgation-item ">
                  <a href="https://www.claro.com.br/claro-clube" title="Conheça o programa de relacionamento Claro clube e todos os benefícios em ser cliente Claro." target="_self" className="m-navgation-item-link gtm-link-event" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Conheça o programa de relacionamento Claro clube e todos os benefícios em ser cliente Claro.">Claro clube</a>
                </li>
                <li className="m-navgation-item">
                  <a href="" title="" className="gtm-link-event m-navgation-item-link" target="_self" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label=""></a>
                </li>
                <li className="m-navgation-item">
                  <a href="https://www.claro.com.br/empresas" title="Conheça os produtos e serviços Claro para sua empresa." className="gtm-link-event m-navgation-item-link" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Conheça os produtos e serviços Claro para sua empresa.">Para sua empresa</a>
                </li>
              </ul>
            </li>
            <li className="m-container-contrate list-submenu">
              <ul className="m-navgation-list breadcrumb submenu-contrate-mobile">
                <li className="submenu-list__item m-navgation-item">
                  <a href="https://planos.claro.com.br" title="Clique aqui e monte um combo Claro de acordo com a sua necessidade." target="_self" className="submenu-list__item--link m-navgation-item-link gtm-link-event icones-fontsize" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Combos Monte um combo de acordo com a sua necessidade.:Clique aqui e monte um combo Claro de acordo com a sua necessidade.">
                  <i className="fa mdn-Icon-carrinho icones-fontsize"></i>
                  <span className="text">Combos Monte um combo de acordo com a sua necessidade.</span>
                  </a>
                </li>
                <li className="submenu-list__item m-navgation-item">
                  <a href="https://assine.claro.com.br/?trks=portalclaro" title="Planos para celular, fale ilimitado e navegue com a internet até 10x mais rápida." target="_self" className="submenu-list__item--link m-navgation-item-link gtm-link-event icones-fontsize" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Planos para celular Fale ilimitado e navegue com a internet até 10x mais rápida.:Planos para celular, fale ilimitado e navegue com a internet até 10x mais rápida.">
                  <i className="fa mdn-Icon-celular icones-fontsize"></i>
                  <span className="text">Planos para celular Fale ilimitado e navegue com a internet até 10x mais rápida.</span>
                  </a>
                </li>
                <li className="submenu-list__item m-navgation-item">
                  <a href="https://planos.claro.com.br" title="Internet, conheça os planos de ultravelocidade para a sua casa." target="_self" className="submenu-list__item--link m-navgation-item-link gtm-link-event icones-fontsize" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Internet Planos de ultravelocidade para a sua casa.:Internet, conheça os planos de ultravelocidade para a sua casa.">
                  <i className="fa mdn-Icon-internet icones-fontsize"></i>
                  <span className="text">Internet Planos de ultravelocidade para a sua casa.</span>
                  </a>
                </li>
                <li className="submenu-list__item m-navgation-item">
                  <a href="https://planos.claro.com.br" title="Planos para TV, A mais completa plataforma de conteúdo para você curtir em todas as telas." target="_self" className="submenu-list__item--link m-navgation-item-link gtm-link-event icones-fontsize" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="TV HD A mais completa plataforma de conteúdo para você curtir em todas as telas.:Planos para TV, A mais completa plataforma de conteúdo para você curtir em todas as telas.">
                  <i className="fa mdn-Icon-tv icones-fontsize"></i>
                  <span className="text">TV HD A mais completa plataforma de conteúdo para você curtir em todas as telas.</span>
                  </a>
                </li>
                <li className="submenu-list__item m-navgation-item">
                  <a href="https://lojaonline.claro.com.br/" title="Os melhores aparelhos em ofertas estão aqui." target="_self" className="submenu-list__item--link m-navgation-item-link gtm-link-event icones-fontsize" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Smartphones Os melhores aparelhos em ofertas estão aqui.:Os melhores aparelhos em ofertas estão aqui.">
                  <i className="fa mdn-Icon-carrinho icones-fontsize"></i>
                  <span className="text">Smartphones Os melhores aparelhos em ofertas estão aqui.</span>
                  </a>
                </li>
                <li className="submenu-list__item m-navgation-item">
                  <a href="https://clarorecarga.claro.com.br/" title="Não fique sem créditos, faça uma recarga para seu pré, controle ou tv." target="_self" className="submenu-list__item--link m-navgation-item-link gtm-link-event icones-fontsize" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Recarga Não fique sem créditos, faça uma recarga.:Não fique sem créditos, faça uma recarga para seu pré, controle ou tv.">
                  <i className="fa mdn-Icon-recarga icones-fontsize"></i>
                  <span className="text">Recarga Não fique sem créditos, faça uma recarga.</span>
                  </a>
                </li>
                <li className="submenu-list__item m-navgation-item">
                  <a href="https://www.claro.com.br/empresas" title="Clique aqui para ser direcionado para o site Claro empresas e conhecer os planos para a sua empresa." target="_self" className="submenu-list__item--link m-navgation-item-link gtm-link-event icones-fontsize" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Para sua empresa Monte um combo para sua empresa.:Clique aqui para ser direcionado para o site Claro empresas e conhecer os planos para a sua empresa.">
                  <i className="fa mdn-Icon-empresa icones-fontsize"></i>
                  <span className="text">Para sua empresa Monte um combo para sua empresa.</span>
                  </a>
                </li>
              </ul>
            </li>
            <li className="m-container-minha-claro list-submenu">
              <ul className="m-navgation-list breadcrumb submenu-minhaClaro-mobile">
                <li className="submenu-list__item m-navgation-item">
                  <a href="https://www.claro.com.br/mc" title="Clique aqui e aproveite todas as facilidades que o Minha Claro móvel oferece." target="_self" className="submenu-list__item--link m-navgation-item-link gtm-link-event icones-fontsize" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Minha Claro móvel:Clique aqui e aproveite todas as facilidades que o Minha Claro móvel oferece.">
                  <i className="fa mdn-Icon-celular icones-fontsize"></i>
                  <span className="text">Minha Claro móvel</span>
                  </a>
                </li>
                <li className="submenu-list__item m-navgation-item">
                  <a href="https://www.claro.com.br/mcr" title="Clique aqui e aproveite todas as facilidades que o Minha Claro residencial (antigo Minha NET) oferece." target="_self" className="submenu-list__item--link m-navgation-item-link gtm-link-event icones-fontsize" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Minha Claro residencial Antigo Minha NET:Clique aqui e aproveite todas as facilidades que o Minha Claro residencial (antigo Minha NET) oferece.">
                  <i className="fa mdn-Icon-residencial icones-fontsize"></i>
                  <span className="text">Minha Claro residencial Antigo Minha NET</span>
                  </a>
                </li>
                <li className="submenu-list__item m-navgation-item">
                  <a href="https://minhaclarotv.claro.com.br" title="Clique aqui e aproveite todas as facilidades que o Minha Claro residencial (antigo Minha Claro tv) oferece." target="_self" className="submenu-list__item--link m-navgation-item-link gtm-link-event icones-fontsize" data-gtm-event-action="clique:menu-superior" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Minha Claro residencial Antigo Minha Claro tv:Clique aqui e aproveite todas as facilidades que o Minha Claro residencial (antigo Minha Claro tv) oferece.">
                  <i className="fa mdn-Icon-residencial icones-fontsize"></i>
                  <span className="text">Minha Claro residencial Antigo Minha Claro tv</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
      <aside className="aside-city target-list">
        <span className="fa icon-close aside-close icones-fontsize"></span>
        <form id="formPreHomeMobile" className="aside-form requestJson forms">
          <fieldset>
            <legend className="hide">Escolha sua Cidade</legend>
            <label className="hide" htmlFor="getValueAutocompleteMobile">Escolha sua Cidade</label>
            <input id="getValueAutocompleteMobile" placeholder="Escolha sua Cidade" className="aside-input inputAutocomplete" type="text" />
            <div className="c-header-btn">
              <button type="submit" className="alterar-cookie c-header-btn-item red" data-home-type="cliente" data-home-url="http://www.claro.com.br/sou-cliente" data-expiration="365">Alterar</button>
            </div>
          </fieldset>
        </form>
      </aside>
      <div className="modal  fade  componente-1509129634198" tabIndex="-1" role="dialog" aria-labelledby="modalComponente1509129634198">
        <div className="modal-dialog modal-lg" role="document" style={{width: '800px', height: '600px'}}>
          <div className="modal-content">
            <a href="#" className="fa fa-times close gtm-link-event" data-dismiss="modal" aria-label="fechar" data-gtm-event-category="claro:portal" data-gtm-event-action="clique:modal:fechar" data-gtm-event-label="fechar"></a>
            <div className="modal-header">
              <h2 id="modalComponente1509129634198" className="modal-title">
                Modal Claro
              </h2>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-sm-12 c-description">
                  <div>
                    <p><strong>Tecnologia 4GMax:</strong>&nbsp;a Claro foi a primeira operadora a trazer para o Brasil uma internet ainda mais r&aacute;pida: o 4G. Tudo para voc&ecirc; ter sempre a melhor experi&ecirc;ncia de navega&ccedil;&atilde;o.</p>
                    <p><strong>Tecnologia 3Gmax:</strong>&nbsp;o 3GMax libera a capacidade m&aacute;xima de transmiss&atilde;o de dados existente na tecnologia 3G. Com ele, voc&ecirc; pode atingir o dobro da velocidade em internet m&oacute;vel.</p>
                    <p><strong>Tecnologia 3G:</strong>&nbsp;a tecnologia de terceira gera&ccedil;&atilde;o &eacute; uma evolu&ccedil;&atilde;o do GSM que oferece servi&ccedil;os multim&iacute;dia com mais velocidade de dados.</p>
                    <p><strong>Tecnologia GSM/2G:</strong>&nbsp;&eacute; a primeira tecnologia 100% digital de telefonia m&oacute;vel. Ela permite que voc&ecirc; utilize diversos servi&ccedil;os para facilitar o seu dia a dia.</p>
                  </div>
                </div>
                <div className="block-container" data-componente="imagemComponente">
                  <a href="" className="gtm-link-event urlToStore" title="" data-gtm-event-action="clique:imagem-modal" data-gtm-event-label="" data-gtm-event-category="claro:portal">
                    <figure>
                      <img className="img-responsive" src="https://www.claro.com.br/imagem/img_chamadadestaquehorizontalcomponente-1509128338504.png" alt="Imagem Lixo Eletrônico" />
                    </figure>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  </nav>
)

export default Nav
