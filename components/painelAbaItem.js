import React from 'react'
import Components from './index'
import SbEditable from 'storyblok-react'

export default (props) => (
  <SbEditable content={props.content}>
  <li className="tab-item-nav">
    <div className="tab-item" tabIndex="0" title="Móvel" aria-controls="aba-1" role="button" aria-expanded="true">
      <div aria-hidden="true">
        <img src="https://www.claro.com.br/imagem/movel-1509138949438.jpg" alt="Móvel" />
      </div>
      <div className="conteudo_txt">
        <h2 className="titulo bind-accordion c-tab titulo_vertical" aria-label="Móvel. Fale e navegue ilimitado e&nbsp;tenha os melhores apps gr&aacute;tis no seu celular.
          ">{props.content.titulo} <span aria-hidden="true" className="icon-rule"><i className="icon-down-custom" aria-hidden="true"></i></span></h2>
        <p className="sub-titulo" aria-hidden="true">{props.content.sub_titulo}
        </p>
      </div>
    </div>
    <div id="aba-1" className="conteudo_desc accordion-content-item">
      <div className="content-item item_1  active">
        <div className="img">
          <img src={props.content.imagen} alt="Conheça os planos Claro pós, controle e pré para celular." />
        </div>
        <div className="bg">
          <div className="text">
            <div>{props.content.text}</div>
          </div>
          <div className="wrap-btn">
            <a href={props.content.link} target="_self" title="Conheça os planos Claro pós, controle e pré para celular." className="c-button botao  gtm-link-event" role="button" data-gtm-event-action="clique:painel-abas-faq" data-gtm-event-label="Conheça os planos Claro pós, controle e pré para celular." data-gtm-event-category="net:portal:/nao-cliente">
            Saiba mais
            </a>
          </div>
        </div>
      </div>
    </div>
  </li>
  </SbEditable>
)
