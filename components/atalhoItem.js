import React from 'react'
import Components from './index'
import SbEditable from 'storyblok-react'

export default (props) => (
  <SbEditable content={props.content}>
  <div className="cms-ShorcutContainer">
    <a href="https://www.claro.com.br/encontre-uma-loja" target="_self" className="mdn-Shortcut  gtm-link-event" title="Encontre uma loja Claro perto de você" data-gtm-event-action="clique:atalho" data-gtm-event-category="claro:portal:/celular/portabilidade" data-gtm-event-label="Ative-seu-prezao">
      <i className="mdn-Shortcut-icon mdn-Icon-loja mdn-Icon--md" aria-hidden="true"></i>
      <h2 className="mdn-Shortcut-text">{props.content.text}</h2>
    </a>
  </div>
  </SbEditable>
)