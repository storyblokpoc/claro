import React from 'react'
import Components from './index'
import SbEditable from 'storyblok-react'

export default (props) => (
  <SbEditable content={props.content}>
    <section className="sectionComponent init-carousel combinacao1 " data-componente="CarrosselPadrao" data-require="CarrosselPadrao" data-number-component="1509138810769">
      <div className="container-fluid CarrosselPadrao">
        <div className="row">
          <div className="owl-carousel owl-theme" data-owl-loop="true" data-owl-autoplay="true">
            {props.content.items.map((blok) =>
              Components(blok)
            )}
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-12">
        </div>
      </div>
    </section>
  </SbEditable>
)