import React from 'react'
import Components from './index'
import SbEditable from 'storyblok-react'

export default (props) => (
  <SbEditable content={props.content}>
  <section className="sectionComponent atalho" data-component-id="1509138837676">
    <div className="container">
      <div className="row">
        <div className="col-sm-12">
          <h2 className="main-title   margin">{props.content.title}</h2>
          <h3 className="main-subtitle   hide">Subtitulo do Componente Atalho</h3>
          <div className="mdn-Row">
            {props.content.items.map((blok) =>
              Components(blok)
            )}
          </div>
        </div>
      </div>
    </div>
  </section>
  </SbEditable>
)