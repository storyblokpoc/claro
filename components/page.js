import React from 'react'
import Link from 'next/link'
import Components from './index'
import SbEditable from 'storyblok-react'

export default (props) => (
  <SbEditable content={props.content}>
    <main className="main-content" id="main-content">
      <div className="container">
        <div className="row">
          <div className="col-sm-12">
            <div className="main-section-wrap">
              <h1 className="off">Portabilidade para Claro</h1>
            </div>
          </div>
        </div>
      </div>
      {props.content && props.content.body && props.content.body.map((blok) =>
        Components(blok)
      )}
    </main>
  </SbEditable>
)