import React from 'react'
import Components from './index'
import SbEditable from 'storyblok-react'

let currentDate = Date.now()

export default (props) => Date.parse(props.content.inicio) < currentDate && Date.parse(props.content.fim) > currentDate && props.content.conteudo.map((blok) =>
  Components(blok)
)