import React from 'react'
import Components from './index'
import SbEditable from 'storyblok-react'

export default (props) => (
  <SbEditable content={props.content}>
  <div className="linear-itens owl-i-flex c-bg">
    <div className="owl-i-content">
      <picture className="zigzag-image linear c-image">
        <source media="(min-width: 1024px)" srcSet={props.content.image} />
        <img src={props.content.image} alt="Faça uma recarga a partir de R$30 e ganhe 1GB de bônus válido por 7 dias." />
      </picture>
      <div className="owl-content">
        <h2 className="zigzag-title c-title">{props.content.title}</h2>
        <div className="c-description owl-flex-align">
          <div>{props.content.description}</div>
        </div>
        <div className="wrap-btn">
          <a href="https://www.claro.com.br/celular/planos-pre/claro-recarga" target="_self" title="Não fique sem créditos, faça agora sua recarga." className="btn-zigzag botao c-button gtm-link-event " role="button" data-gtm-event-action="clique:zigzag-linear" data-gtm-event-category="claro:portal:/celular/planos-pre" data-gtm-event-label="saibamais-destaque-recarga">
          {props.content.button_label || 'Saiba mais'}
          <i className="icon fa "></i>
          </a>
        </div>
      </div>
    </div>
  </div>
  </SbEditable>
)