import React from 'react'
import Components from './index'
import SbEditable from 'storyblok-react'

export default (props) => (
  <SbEditable content={props.content}>
  <section className="chamada-zigzag-linear sectionComponent" data-require="ZigZag" data-number-component="1509130754694">
    <div className="container">
      <div className="row">
        <div className="col-sm-12 combinacao1 owl-flex component-zigzag">
          <h2 className="main-title corDefault margin">{props.content.title}</h2>
          <h3 className="main-subtitle corDefault hide">subtitulo do componente ZigZag</h3>
          <div className="wrap-chamada-zigzag-linear">
            <div className="owl-flex-wrap align-text-left align-button-left" data-swipe="zigzag-linear">
              {props.content.items.map((blok) =>
                Components(blok)
              )}
            </div>
            <div className="owl-nav"></div>
          </div>
        </div>
        <div className="col-sm-12">
        </div>
      </div>
    </div>
    <div className="zigzag-modais">
    </div>
  </section>
  </SbEditable>
)