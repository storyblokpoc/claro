import React from 'react'
import Components from './index'
import SbEditable from 'storyblok-react'

let cidade = 'Rio de Janeiro'

export default (props) => props.content.cidades.indexOf(cidade) > -1 && props.content.items.map((blok) =>
  Components(blok)
)