import React from 'react'
import Components from './index'
import SbEditable from 'storyblok-react'

export default class extends React.Component {
  resizedIcon(size) {
    const { content } = this.props
    if (typeof content.image !== 'undefined') {
      return content.image.replace('//img2.storyblok.com/' + size, '//a.storyblok.com')
    }
    return null
  }

  render() {
    const { content } = this.props
    return (
      <SbEditable content={content}>
        <a href="https://www.clarogaming.gg/" className="caption gtm-link-event" title="Seu próximo novo é ter o pack de vantagens que vai muito além do jogo. São muitos benefícios powered by gamers. Saiba mais sobre o Claro Gaming!" target="_blank" data-gtm-event-action="clique:carrossel:CarrosselPadrao" data-gtm-event-category="claro:portal:" data-gtm-event-label="banner-gaming">
          <div className="item" data-color="">
            <picture aria-hidden="true">
              <source media="(min-width: 1024px)" srcSet={this.resizedIcon('1024x0')} />
              <source media="(min-width: 650px)" srcSet={this.resizedIcon('650x0')} />
              <source media="(min-width: 300px)" srcSet={this.resizedIcon('300x0')} />
              <img src={this.resizedIcon('1920x0')} alt="Seu próximo novo é ter o pack de vantagens que vai muito além do jogo. São muitos benefícios powered by gamers. Saiba mais sobre o Claro Gaming!" data-gtm-event-action="clique:carrossel:CarrosselPadrao" data-gtm-event-category="claro:portal:" data-gtm-event-label="banner-gaming" />
            </picture>
          </div>
        </a>
      </SbEditable>
    )
  }
}
