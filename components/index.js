import React from 'react'
import carousel from './carousel'
import carouselItem from './carouselItem'
import zigtagItem from './zigzagItem'
import Zigtag from './zigzag'
import painelAbas from './painelAbas'
import painelAbaItem from './painelAbaItem'
import atalho from './atalho'
import atalhoItem from './atalhoItem'
import segmento from './segmento'
import agendador from './agendador'
import page from './page'

const Components = {
  'carousel': carousel,
  'carouselItem': carouselItem,
  'zigzag': Zigtag,
  'zigzagItem': zigtagItem,
  'painelAbas': painelAbas,
  'painelAbaItem': painelAbaItem,
  'atalho': atalho,
  'atalhoItem': atalhoItem,
  'segmento': segmento,
  'agendador': agendador,
  'page': page
}

export default (blok) => {
  if (typeof Components[blok.component] !== 'undefined') {
    return React.createElement(Components[blok.component], { key: blok._uid, content: blok })
  }
  return React.createElement(() => (
    <div>The component {blok.component} has not been created yet.</div>
  ), {key: blok._uid})
}