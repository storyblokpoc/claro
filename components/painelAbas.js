import React from 'react'
import Components from './index'
import SbEditable from 'storyblok-react'

export default (props) => (
  <SbEditable content={props.content}>
  <section className="abas_verticais combinacao1 sectionComponent accordion" data-require="AbasVerticais">
    <div className="container">
      <div className="row">
        <div className="col-sm-12">
          <h2 className="main-title margin">{props.content.title}</h2>
        </div>
        <div className="col-sm-12 conteudo wrap-accordion">
          <ul className="conteudo_aba accordion-content" aria-label="Pressione a tecla Enter para expandir um ítem da lista">
            {props.content.items.map((blok) =>
              Components(blok)
            )}
          </ul>
        </div>
      </div>
    </div>
  </section>
  </SbEditable>
)