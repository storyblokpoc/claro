## Setup

Os passos iniciais para o setup do projeto e entendimento como a Storyblok funciona, pode ser lido no seguinte tutorial: https://www.storyblok.com/tp/next-js-react-guide

## Regionalização (segmentação)

### User Story:

Como EDITOR DE CONTEÚDO quero poder cadastrar um banner no componente
carrossel, escolhendo quais cidades ele será exibido, porque deste modo, consigo ofertar
valores diferentes para regiões distintas.

### Solução:

Para reutilizar essa função em outros lugares foi criado com componente chamado "Segmento" que tem dois campos. Um para selecionar as cidades e um para inserir outros componentes.

![Screenshot](https://img2.storyblok.com/800x0/f/70292/824x427/70b14824b7/screenshot-2019-12-02-at-19-49-10.jpg)

A estrutura permite grupamento de conteúdos:

![Screenshot](https://img2.storyblok.com/800x0/f/70292/1138x705/1aa56cd90c/screenshot-2019-12-02-at-19-48-20.jpg)

### User Story:

Como CONSUMIDOR DE API quero poder enviar um parâmetro na requisição
informando a cidade e receber um retorno do conteúdo filtrado para aquela cidade, não exibindo
conteúdos de outras cidades, para que haja flexibilidade na entrega do conteúdo.

### Solução:

Recomendamos que o CONSUMIDOR DE API receba toda informação da página e deixar o renderer cuidar da filtragem de componentes.
No nosso exemplo o componente "segmento" filtra os conteúdos recebidos pelo API e somente mostra os conteúdos da cidade selecionado:

```javascript
let cidade = 'Rio de Janeiro'

export default (props) => props.content.cidades.indexOf(cidade) > -1 && props.content.items.map((blok) =>
  Components(blok)
)
```

## Preview de conteúdo

### User Story:

Como EDITOR DE CONTEÚDO quero poder visualizar minhas alterações no CMS
aplicadas ao layout atual do site por que preciso validar minhas alterações antes de publicar o
novo conteúdo.

### Solução:

Para o "Live preview" da Storyblok, basta adicionar o componente ```<SbEditable>``` em cada parte do template e incluir o Javascript bridge no arquivo ```[slug].js```.
Assim que usuário muda o conteúdo o preview automaticamente se atualiza.

![](https://img2.storyblok.com/800x0/f/70292/1262x780/868471f59b/screenshot-2019-12-02-at-20-03-54.jpg)

## Busca de conteúdo

### User Story:

Como EDITOR DE CONTEÚDO quero poder buscar imagens, componentes, textos e
links de maneira rápida e eficaz, utilizando palavras chaves, filtros por tipo de conteúdo, data de
expiração/publicação ou tags previamente cadastradas, para que melhore a minha produtividade
no dia a dia.

A partir desta estória, podemos determinar dois critérios de aceite:

1. Na busca por imagem, é indispensável uma opção para a exibição de um thumbnail da
imagem, com a finalidade de agilizar o cadastro.
2. A busca deve possuir uma opção avançada, contendo busca por mais campos ou tipos
de conteúdo.

### Solução:

Existem duas opções para mostrar o thumbnail da imagen na busca:

![](https://img2.storyblok.com/800x0/f/70292/1174x585/ae14ec8b3f/screenshot-2019-12-02-at-20-41-57.jpg)

Existem filtros para campos especificos, tipos de conteúdo, tags, e mais:

![](https://img2.storyblok.com/800x0/f/70292/1177x590/311a6d57ba/screenshot-2019-12-02-at-20-42-53.jpg)

## Histórico de trabalho (versionamento)

### User Story:

Como EDITOR DE CONTEÚDO quero poder visualizar o histórico de trabalho
completo, exibindo quem alterou, quando foi alterado e o que foi alterado, com a possibilidade
de recuperar uma versão anterior do conteúdo, para que tenhamos um melhor controle do
conteúdo exibido no site e do dia a dia de trabalho.

### Solução:

O histórico de trabalho da Storyblok mostra cada mudança que foi feito com possibilidade de recuperar as versões antigas.

![](https://img2.storyblok.com/800x0/f/70292/1259x599/767d830531/screenshot-2019-12-02-at-20-49-45.jpg)

## Workflow de aprovação e níveis de acesso

### User Story:

Como PUBLICADOR DE CONTEÚDO quero poder validar as alterações feitas pelo
EDITOR DE CONTEÚDO antes de aprovar para publicação, porque preciso garantir a qualidade
do conteúdo exibido no site.

A partir desta estória, podemos determinar dois critérios de aceite:

1. É necessário um preview das alterações em ambiente STAGING (Homologação) antes da
publicação para PRODUÇÃO.
2. É importante ter uma área para comentários sobre o conteúdo enviado para validação.

### Solução:

Através de "Branches" é possivel publicar em ambiente Staging: https://www.storyblok.com/docs/setup-branches-and-releases

![](https://img2.storyblok.com/800x0/f/70292/1234x618/983624f5db/screenshot-2019-12-02-at-20-57-53.jpg)

Através de "User roles" também é possivel definir um grupo de usuários que podem fazer a publicação em cada ambiente.

![](https://img2.storyblok.com/800x0/f/70292/1246x623/3094001761/screenshot-2019-12-02-at-20-59-00.jpg)

Com workflows https://www.storyblok.com/docs/workflows é possivel forcar os usuários para seguir um processo de validação e criar comentários.

## REQUISITOS ADICIONAIS

## Cadastro ágil

### User Story:

Como EDITOR DE CONTEÚDO quero poder cadastrar um novo item do banner
carrossel contendo imagem, link e texto acessível tudo em apenas uma tela, de maneira fácil e
intuitiva, porque há abertura de uma janela para cadastrar imagem, outra para cadastrar link e
outra para o banner diminuir a produtividade.

### Solução:

Com o editor de blocos é super fácil cadastrar todo contéudo da página sem sair da tela.
Para facilitar o trabalho do editor ainda mais, é possivel cadastrar "Presets" de components que deixa os componentes já preenchidos.

![](https://img2.storyblok.com/800x0/f/70292/987x619/7dc113ebee/screenshot-2019-12-02-at-21-13-44.jpg)

## Agendamento/expiração

### User Story:

Como EDITOR DE CONTEÚDO quero poder cadastrar conteúdos com data de
publicação ou expiração, para que eu possa exibir um conteúdo, uma imagem ou oferta durante
um período específico.

### Solução:

A recommendação aqui é usar o mesmo schema do componente "Segmento", mas dessa vez o componente "Agendador".

![](https://img2.storyblok.com/800x0/f/70292/1167x653/e4f0c59f5d/screenshot-2019-12-02-at-21-39-14.jpg)

```javascript
let currentDate = Date.now()

export default (props) => Date.parse(props.content.inicio) < currentDate && Date.parse(props.content.fim) > currentDate && props.content.conteudo.map((blok) =>
  Components(blok)
)
```