import React from 'react'
import Head from 'next/head'
import Components from '../components/index'
import Nav from '../components/nav'
import StoryblokService from '../utils/storyblokService'

export default class extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      pageContent: props.page.data.story.content,
    }
  }

  static async getInitialProps({ query }) {
    StoryblokService.setQuery(query)

    return {
      page: await StoryblokService.get('cdn/stories/' + query.slug)
    }
  }

  componentDidMount() {
    StoryblokService.initEditor(this)
  }

  render() {
    return (
      <div>
        <Head>
          <title>Home</title>
          <link rel="icon" href="/favicon.ico" />
          <link rel="stylesheet" href="https://www.claro.com.br/static/css/main.css?v1.0.31" />
          <link href="https://mondrian.claro.com.br/icons/mondrian-icons-latest.css" rel="stylesheet" />
          <script type="text/javascript" src="/claro.js"></script>
        </Head>

        <Nav />
        {Components(this.state.pageContent)}
        {StoryblokService.bridge()}
      </div>
    )
  }
}
